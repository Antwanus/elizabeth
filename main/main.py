import datetime as dt
import smtplib

MY_EMAIL = "antwanus1337@gmail.com"
TO_EMAIL = [MY_EMAIL, "stdepestel@hotmail.com"]
MY_PASSWORD = "geheimpje"
TODAY = dt.date.today()
BDAY_ELIZABETH = dt.date(year=2021, month=7, day=8)
LEAPS_TIMEDELTA = {
    1: (dt.timedelta(weeks=4.5), dt.timedelta(weeks=5.5), 'wereld van sensaties'),
    2: (dt.timedelta(weeks=7.5), dt.timedelta(weeks=9.5), 'wereld van patronen'),
    3: (dt.timedelta(weeks=11.5), dt.timedelta(weeks=12.5), 'wereld van vloeiende overgangen'),
    4: (dt.timedelta(weeks=14.5), dt.timedelta(weeks=19.5), 'wereld van gebeurtenissen'),
    5: (dt.timedelta(weeks=22.5), dt.timedelta(weeks=26.5), 'wereld van relaties'),
    6: (dt.timedelta(weeks=33.5), dt.timedelta(weeks=37.5), 'wereld van categorieën'),
    7: (dt.timedelta(weeks=41.5), dt.timedelta(weeks=46.5), 'wereld van opeenvolgingen'),
    8: (dt.timedelta(weeks=50.5), dt.timedelta(weeks=54.5), 'wereld van programma\'s'),
    9: (dt.timedelta(weeks=59.5), dt.timedelta(weeks=64.5), 'wereld van principes'),
    10: (dt.timedelta(weeks=70.5), dt.timedelta(weeks=75.5), 'wereld van systemen'),
}

newer_list = [(key, BDAY_ELIZABETH + tupl[0], BDAY_ELIZABETH + tupl[1], tupl[2])
              for (key, tupl) in LEAPS_TIMEDELTA.items()]

for (leap_number, start_leap_date, end_leap_date, description) in newer_list:
    # if today is in range of (start_leap_date -7 days) ... end_leap_date
    if start_leap_date - dt.timedelta(weeks=1) <= TODAY <= end_leap_date:
        diff_in_days = (TODAY - start_leap_date).days
        # if difference between the dates is negative (= earlier)
        if diff_in_days < 0:
            with smtplib.SMTP("smtp.gmail.com", port=587) as connection:
                connection.starttls()
                connection.login(MY_EMAIL, MY_PASSWORD)
                connection.sendmail(
                    from_addr=MY_EMAIL,
                    to_addrs=MY_EMAIL,
                    msg=f"Subject:Sprongetje Elizabeth"
                        f"\n\nVandaag is het {TODAY}."
                        f"\nOns bieke haar sprongetje start over {diff_in_days * -1} dagen!"
                        f"\nBinnenkort ontdekt ons Bieke de {description}!!"
                        f"\nDe sprong begint {start_leap_date} & eindigt {end_leap_date}"
                )
